using PropertyChanged;
using WPFTemplateLib.Mvvm;

namespace OxyPlotTester.Utils
{
	/// <summary>
	/// ViewModel 基类
	/// </summary>
    public abstract class MyViewModelBase : ViewModelBase
	{
		/// <inheritdoc />
		protected override bool IsSetConsoleOutput { get; set; } = true;

		protected MyViewModelBase ()
		{
		}
	}
}
